const BASE_URL = "https://643a58d490cd4ba563f7754c.mockapi.io/food";

export class Service {
  getFood = () => {
    return axios({
      url: `${BASE_URL}`,
      method: "GET",
    });
  };

  getFoodById = (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  };

  addFood = (data) => {
    return axios({
      url: `${BASE_URL}`,
      method: "POST",
      data: data,
    });
  };

  deleteFood = (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  };

  updateFood = (id, data) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: data,
    });
  };
}
