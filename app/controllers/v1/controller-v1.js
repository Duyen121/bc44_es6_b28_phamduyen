import { Food } from "../../models/v1/model.js";

let formInputs = [
  "foodID",
  "tenMon",
  "loai",
  "giaMon",
  "khuyenMai",
  "tinhTrang",
  "hinhMon",
  "moTa",
];

export class Controller {
  getEle = (id) => {
    return document.getElementById(id);
  };

  getFormInfo = () => {
    var inputValues = formInputs.map((input) => {
      return this.getEle(input).value;
    });
    var food = new Food(...inputValues);

    return food;
  };
}
