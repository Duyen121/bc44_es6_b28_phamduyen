import { renderFoodList } from "./controller-v2.js";
import { Food } from "../../models/v1/model.js";
import { Controller } from "../../controllers/v1/controller-v1.js";
import { Service } from "../../services/service.js";

const service = new Service();
const controller = new Controller();
let idSelected = "";

window.resetInput = () => {
  $(":input", "#exampleModal").val("").prop("selectedIndex", 0);
  controller.getEle("btnThemMon").style.display = "inline-block";
  $("#btnCapNhat").hide();
};

let fetchFoodList = () => {
  service
    .getFood()
    .then((res) => {
      let foodArr = res.data.map((item) => {
        let { id, name, type, discount, img, desc, price, status } = item;
        let food = new Food(id, name, type, price, discount, status, img, desc);
        return food;
      });
      renderFoodList(foodArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

fetchFoodList();

let deleteFood = (id) => {
  service
    .deleteFood(id)
    .then((res) => {
      fetchFoodList();
    })
    .catch((err) => {});
};

window.deleteFood = deleteFood;

window.addFood = () => {
  let data = controller.getFormInfo();
  let newFood = {
    name: data.name,
    type: data.type,
    discount: data.discount,
    img: data.img,
    desc: data.desc,
    price: data.price,
    status: data.status,
  };

  service
    .addFood(newFood)
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList();
    })
    .catch((err) => {});
};

window.editFood = (id) => {
  idSelected = id;
  service
    .getFoodById(id)
    .then((res) => {
      $("#exampleModal").modal("show");
      $("#btnCapNhat").show();
      controller.getEle("btnThemMon").style.display = "none";

      // show info onto form
      let { id, name, type, discount, img, desc, price, status } = res.data;

      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value =
        type == "Chay" ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;
      document.getElementById("tinhTrang").value = status;
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = desc;
    })
    .catch((err) => {});
};

window.updateFood = () => {
  let data = controller.getFormInfo();
  service
    .updateFood(idSelected, data)
    .then(function (res) {
      $("#exampleModal").modal("hide");
      fetchFoodList();
    })
    .catch(function (err) {});
};
