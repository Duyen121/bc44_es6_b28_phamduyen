var vndFormat = new Intl.NumberFormat("vi-VN", {
  style: "currency",
  currency: "VND",
  maximumFractionDigits: 0,
  minimumFractionDigits: 0,
});

export let renderFoodList = (foodList) => {
  let HTMLContent = "";

  foodList.forEach((item) => {
    let { id, name, type, price, discount, status } = item;
    let trContent = `<tr>
        <td>${id}</td>
        <td>${name}</td>
        <td>${type == "loai1" ? "Chay" : "Mặn"}</td>
        <td>${vndFormat.format(price)}</td>
        <td>${discount}%</td>
        <td>${vndFormat.format(item.countDiscount())}</td>
        <td>${status == 1 ? "Còn" : "Hết"}</td>
        <td>
        <button class="btn btn-warning" onclick="editFood(${id})">Edit</button>
        <button class="btn btn-danger" onclick="deleteFood(${id})">Delete</button>
        </td>
        </tr>`;
    HTMLContent += trContent;
  });
  document.getElementById("tbodyFood").innerHTML = HTMLContent;
};
